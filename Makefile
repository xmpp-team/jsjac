OUTFILE=./jsjac.js
PACKFILE=./jsjac.packed.js
UNCOMPRESSED=./jsjac.uncompressed.js
SRC=src/jsextras.js src/crypt.js src/JSJaCJSON.js src/xmlextras.js \
src/JSJaCBuilder.js src/JSJaCConstants.js \
src/JSJaCConsoleLogger.js src/JSJaCCookie.js src/JSJaCError.js \
src/JSJaCJID.js src/JSJaCKeys.js src/JSJaCPacket.js src/JSJaCConnection.js \
src/JSJaCHttpBindingConnection.js src/JSJaCHttpPollingConnection.js \
src/JSJaC.js

all: clean pack install doc

install: build uncompressed
	@echo "done."

build: 
	@echo "building ...";
	@for i in ${SRC}; do \
		echo "\t$$i"; \
		cat "$$i" >> $(OUTFILE); \
	done

pack: clean build moo doc

moo:	
	@echo "packing..."
	@if [ -e $(OUTFILE) ]; then \
		php ./utils/packer/pack.php $(OUTFILE) $(PACKFILE).tmp && \
		cat src/header.js > $(PACKFILE) && \
		cat src/JSJaCConfig.js >> $(PACKFILE) && \
		cat $(PACKFILE).tmp >> $(PACKFILE) && \
		rm $(PACKFILE).tmp; \
	else \
		echo "$(OUTFILE) not found. build failed?"; \
	fi

doc: 
	@utils/JSDoc/jsdoc.pl --project-name JSJaC -d doc src/

clean:
	@rm -f $(OUTFILE) 2>/dev/null
	@rm -f $(PACKFILE) 2>/dev/null
	@rm -f $(UNCOMPRESSED) 2>/dev/null
	@rm -rf doc/

uncompressed: 
	@if [ -e $(OUTFILE) ]; then \
		cat src/header.js > $(UNCOMPRESSED) && \
		cat src/JSJaCConfig.js >> $(UNCOMPRESSED) && \
		cat $(OUTFILE) >> $(UNCOMPRESSED); \
	fi

.PHONY: doc utils
